# onlyoffice-desktopeditors

Free office suite that combines text, spreadsheet and presentation editors

https://github.com/ONLYOFFICE/DesktopEditors

https://gitlab.manjaro.org/packages/community/onlyoffice-desktopeditors

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/writing-tools/onlyoffice-desktopeditors.git
```

