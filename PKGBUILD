# Maintainer: Philip Mueller <philm[at]manjaro[dot]org>
# Maintainer: Mark Wagie <mark@manjaro.org>
# Contributor: Helmut Stult

# Contributor: David Spink <yorperprotonmailcom>

pkgname=onlyoffice-desktopeditors
pkgver=7.2.0
pkgrel=1
pkgdesc="Free office suite that combines text, spreadsheet and presentation editors allowing to create, view and edit documents without an Internet connection. "
arch=('x86_64')
url="https://www.onlyoffice.com/"
urldown="https://github.com/ONLYOFFICE/DesktopEditors/releases/download"
license=('AGPL3')
depends=('curl' 'gtk3' 'alsa-lib' 'libpulse' 'gstreamer' 'gst-plugins-base-libs'
        'gst-plugins-ugly' 'libxss' 'nss' 'nspr' 'ttf-dejavu' 'ttf-liberation'
        'ttf-carlito' 'desktop-file-utils' 'hicolor-icon-theme')
optdepends=('libreoffice: for OpenSymbol fonts')
#            'otf-takao: for japanese Takao fonts' # Only available in the AUR
#            'ttf-ms-fonts: for Microsoft fonts' # Only available in the AUR
provides=('onlyoffice')
conflicts=('onlyoffice' 'onlyoffice-bin')
source=("${pkgname}-${pkgver}-x64.tar.gz::${urldown}/v${pkgver}/${pkgname}-x64.tar.gz"
        'org.onlyoffice.desktopeditors.desktop'
        'onlyoffice')
noextract=("${pkgname}-${pkgver}-x64.tar.gz")
sha256sums=('ff26b7784bb83a86e3d4e2d0621ae90c8eb150fa91b5a5e6c984d425266c259f'
            'df4fc7a175408b49ff7f00394104750103d2f13371f5405fdf5bebca78083d2c'
            '719ea444786d7c134f4c640b03ccae3cc8ff0229900aa1098b170ae1169273ad')

package() {
  # extract content
  install -d "${pkgdir}"/{opt/onlyoffice,usr/{bin,share/licenses/"${pkgname}"}}
  bsdtar -zxvf ${pkgname}-${pkgver}-x64.tar.gz -C "${pkgdir}"/opt/onlyoffice

  # icons
    local _file
    local _res
    while read -r -d '' _file
    do
        _res="$(sed 's/\.png$//;s/^.*-//' <<< "$_file")"
        install -d "${pkgdir}/usr/share/icons/hicolor/${_res}x${_res}/apps"
        ln -s "../../../../../../opt/onlyoffice/desktopeditors/asc-de-${_res}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${_res}x${_res}/apps/asc-de.png"
    done < <(find "${pkgdir}/opt/onlyoffice/desktopeditors" -maxdepth 1 -type f -name 'asc-de-*.png' -print0)

  # install script, desktop and license files
  install -Dm755 onlyoffice -t "${pkgdir}/usr/bin"
  install -Dm644 org.onlyoffice.desktopeditors.desktop -t "${pkgdir}/usr/share/applications"

  ln -s /opt/onlyoffice/desktopeditors/LICENSE.htm "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.htm"
  ln -s /opt/onlyoffice/desktopeditors/3DPARTYLICENSE  "${pkgdir}/usr/share/licenses/${pkgname}/3DPARTYLICENSE"
}
